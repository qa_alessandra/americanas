# README #

Projeto Automação Web
Utilizando as ferramentas que preferir crie cenários de testes automatizados que façam o fluxo E2E de um cliente

Ok - 1 - Entre em um grande portal de comércio online (Exemplo: Amazon, Submarino, Magazine Luiza, Americanas). 
OK - 2 - Realize uma busca por um produto.  
OK - 3 - Valide o retorno da busca. 
OK - 4 - Escolha um produto na lista.
OK - 5 - Adicione o carrinho.
OK - 6 - Valide o produto no carrinho.
7 - Pesquise o CEP da Rua: Henri Dunant via API Rest com Rest Assured. 
8 - Adicione o CEP para calcular o frete.
9 - Valide o valor do frete.

Para a criação dos cenários de testes devem ser considerados casos de sucesso e fluxos alternativos(Negativos).

Sugestões de ferramentas para automação: Selenium, Junit, Cucumber...
Linguagens como: Ruby, Java, JavaScript...

Dica: Você pode utilizar a API https://viacep.com.br/ para consulta de CEP.

Lembre-se: Utilizamos boas práticas de organização de código e relatórios automatizados. O seu teste será avaliado de acordo com o modo que você estrutura seus testes e interage com os elementos da aplicação.
 