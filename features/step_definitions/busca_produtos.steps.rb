Dado('que acesso a pagina principal do {string}') do |americanas|
    url = DATA[americanas]    
    visit(url)
end
  
Quando('realizar uma busca do um produto') do
    @busca.campo_busca
end
  
Então('devo ver uma lista de produto disponivel') do
    @busca.validar_lista
end

Quando('realizar uma busca de um produto qualquer') do
    @busca.produto_qualquer
end
  
Então('devo der a mensagem {string}') do |string|
    @busca.validar_mensagem_produto_nao_encontrado

end
  