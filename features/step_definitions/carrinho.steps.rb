Quando('escolho um produto') do
    @busca.campo_busca
    @carrinho.escolhendo_produto
end
  
Quando('adiciono no carrinho') do
    @carrinho.adicionando_produto_carrinho
end
  
Então('então valido o produto adicionado no carrinho') do
    @carrinho.validar_pagina_carrinho
end