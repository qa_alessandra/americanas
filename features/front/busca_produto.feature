#language:pt


Funcionalidade: Pesquisa de um produto 

Contexto: Pagina de busca 
    Dado que acesso a pagina principal do "americanas"


    @busca
    Cenario:  Busca realiza com sucesso 

   
    Quando realizar uma busca de um produto
    Então devo ver uma lista do produto disponivel

    @sem
    Cenario: Busca sem estoque 

    Quando realizar uma busca de um produto qualquer
    Então devo der a mensagem "Ops! nenhum resultado encontrado para"



