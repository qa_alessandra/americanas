require 'capybara'
require 'capybara/cucumber'
require 'faker'
require 'ffaker'
require 'rspec'
require 'selenium-webdriver'
require 'pry'
require 'report_builder'


Capybara.configure do |config|
    config.default_driver = :selenium_chrome
    config.default_max_wait_time = 15
end

DATA = YAML.load_file('./data/data.yml')
EL   = YAML.load_file('./data/elements.yml')