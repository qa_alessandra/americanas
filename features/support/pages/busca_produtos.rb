class Busca

    include RSpec::Matchers
    include Capybara::DSL

    def initialize

        @botao_cookies          = EL['botao_cookies'] 
        @campo_busca            = EL['campo_busca']
        @lupa_busca             = EL['lupa_busca']
        @lista_produto          = EL['lista_produto']
        @mensagem_sem_resultado = EL['mensagem_sem_resultado']

        def campo_busca

            busca = "Celular Motorola G10"

            find(@botao_cookies).click
            find(@campo_busca).set busca
            sleep 5
            find(@lupa_busca).click
     
            
        end

        def validar_lista 
            
            contem_texto = find(@lista_produto).has_content?("g10")

            expect(contem_texto).to be true

        end


        def produto_qualquer

            busca_qualquer =  "kkkkkkkk"

            find(@campo_busca).set busca_qualquer
            find(@lupa_busca).click

        end

        def validar_mensagem_produto_nao_encontrado

            expect(page).to have_text(@mensagem_sem_resultado)
            
        end
        
    end
end