class Carrinho

    include RSpec::Matchers
    include Capybara::DSL

    def initialize
        @imagem_produto             = EL['imagem_produto']
        @botao_comprar              = EL['botao_comprar']
        @botao_produto_escolhido    = EL['botao_produto_escolhido']
        @botao_radio_sem_seguro     = EL['botao_radio_sem_seguro']
        @botao_continuar_carrinho   = EL['botao_continuar_carrinho']
        @titulo_carrinho            = EL['titulo_carrinho']


    end

    def escolhendo_produto

        find(@imagem_produto).click
        find(@botao_comprar).click
        find(@botao_produto_escolhido).click
        find(@botao_radio_sem_seguro).click
       

    end

    def adicionando_produto_carrinho

        find(@botao_continuar_carrinho).click
      
    end

    def validar_pagina_carrinho 

        expect(page).to have_text(@titulo_carrinho)
        
    end

end